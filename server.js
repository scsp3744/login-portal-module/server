// setup
const bodyParser = require('body-parser');
const express = require('express');
const session = require('express-session');
const mongoose = require('mongoose');
const morgan = require('morgan');
const ejs = require('ejs');
const path = require('path');
const app = express();

// database configuration
mongoose.connect('mongodb+srv://root:%23StupidPwS3ttings@ionic1app-orjzy.mongodb.net/loginPortal?retryWrites=true&w=majority', {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
}, (err) => {
    if (err) { console.log('Error connecting to database.'); }
});

app.use(session({
    secret: 'himitsu',
}));

app.use(morgan('dev'));// log every request to the console
// dev - the format we want to use for output

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

// parse application/json
app.use(bodyParser.json());

// to serve HTML directly (for website)
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../website'));

// to allow Ionic GET / POST this server
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// const MongoClient = require('mongodb').MongoClient;
// const uri = "mongodb+srv://root:%23StupidPwS3ttings@ionic1app-orjzy.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

// var db;

// client.connect((err, something) => {
//   // if(err){
//   //     console.log("something happened");
//   //     console.log(err.toString())
//   // }
//   db = something.db('loginPortal');
//   // perform actions on the collection object
//   app.listen(5100, () => console.log('Server started on port 5100'));
//   if (err) {
//     client.close();
//   }
// });

// Models //////////////////////////////////////////////////////////////////////
var userRole = mongoose.model('userRole', {
    role: { type: String }
}, 'userRole');

var user = mongoose.model('user', {
    userRole_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'userRole',
        required: true
    },
    username: { type: String, required: true, unique: true },
    officialEmail: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: { type: String, required: true },
    resetKey: String,
    lastLogin: Number
}, 'user');

var student = mongoose.model('student', {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, required: true },
    matricNo: { type: String, required: true }
}, 'student');

var lecturer = mongoose.model('lecturer', {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, required: true }
}, 'lecturer');

var staff = mongoose.model('staff', {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, required: true }
}, 'staff');


// Routes //////////////////////////////////////////////////////////////////////
app.post('/api/login', (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    //var userRole = req.body.userRole;
    var date = new Date();
    var lastLogin = date.getTime();

    console.log(req.body.userRole);

    user.findOne({ "username": req.body.username, "password": req.body.password }).exec()
        .then(users => {
            console.log("AAAAAAAAAA");
            console.log(req.body.username);
            console.log(users);
            if (users) {
                var now = Date.now()
                user.updateOne({ _id: users._id }, { $set: { lastLogin: now } }).exec()
                userRole.findOne({"_id": users.userRole_id}).exec()
                .then(userRoles => {
                    if (userRoles.role == 'student') {
                        student.findOne({ user_id: users._id }).exec()
                            .then(stud => {
                                console.log(stud, users._id)
                                if (stud) {
                                    return res.status(200).json({ user_id: users._id, userRole: userRoles.role, name: stud.name, matricNo: stud.matricNo });
                                }
                            });
                    }
    
                    if (userRoles.role == 'lecturer') {
                        lecturer.findOne({ user_id: users._id }).exec()
                            .then(lect => {
                                console.log(lect, users._id)
                                if (lect) {
                                    return res.status(200).json({ user_id: users._id, userRole: userRoles.role, name: lect.name });
                                }
                            });
                    }
    
                    if (userRoles.role == 'staff') {
                        staff.findOne({ user_id: users._id }).exec()
                            .then(staf => {
                                console.log(staf, users._id)
                                if (staf) {
                                    return res.status(200).json({ user_id: users._id, userRole: userRoles.role, name: staf.name });
                                }
                            });
                    }
                })
            }
            else {
                return res.status(401).json({ message: "Username / password invalid." });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err, message: "Auth failed." });
        });
});

// app.get('/api/dash', function (req, res) {
//   db.collection('lecturer').find({ "user_id": ObjectId }).toArray(function (err, results) {
//     if (results === undefined || results.length == 0) {
//       res.json({
//         message: "Error, please contact CICT"
//       });
//     }
//     else {
//       res.json({ results });
//     }
//   })
// });

// app.get('/api/profile', function (req, res) {
//   db.collection('lecturer').find({ "user_id": ObjectId }).toArray(function (err, results) {
//     if (results === undefined || results.length == 0) {
//       res.json({
//         message: "Error, please contact CICT"
//       });
//     }
//     else {
//       res.json({ results });
//     }
//   })
// });

app.post('/api/changePw', function (req, res) {
    console.log(req.body);
    user.findOne({ username: req.body.username, password: req.body.password })
        .exec()
        .then(users => {
            if (users) {
                user.updateOne({ username: req.body.username }, { $set: { password: req.body.newpassword } }).exec();
                return res.status(200).json({ message: "yes" });
            }
            else {
                return res.status(200).json({ message: "no" });
            }

        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
});

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'banadody25@gmail.com',
        pass: 'LV7}vGx&t4UD8sa`'
    }
});

app.post('/api/forgotPw', function (req, res) {
    user.findOne({ "officialEmail": req.body.officialEmail })
        .exec()
        .then(users => {
            if (users) {
                var ResetKey = makeid();
                var mailOptions = {
                    from: 'banadody25@gmail.com',
                    to: req.body.officialEmail,
                    subject: 'Password reset link for UTM Smart 2.0',
                    text: 'Please click this link http://localhost:5100/website/resetpw/' + ResetKey
                };
                console.log(mailOptions);
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                        user.updateOne({ officialEmail: req.body.officialEmail }, { $set: { resetKey: ResetKey } }).exec();
                        return res.status(200).json({ message: "Reset password email sent." });
                    }
                });
            }
            else {
                return res.status(200).json({ message: "Please enter the oficial email registered with UTM, please contact CICT if you do not remember it." });
            }
        })
});

app.post('/api/resetpw', (req, res) => {
    user.findOne({ _id: req.body.user_id, resetKey: req.body.ResetKey }).exec()
        .then(doc => {
            if (doc) {
                user.updateOne({ _id: req.body.user_id }, { $set: { password: req.body.newpassword, resetKey: null } }).exec()
                res.status(200).json({ message: 'Password has been reset.' })
            }
            else {
                res.status(200).json({ message: 'Error' })
            }
        })
})

function makeid() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

// Routes for Reset Password Websites /////////////////////////////////////////////////////////

app.get('/website/resetpw/:ResetKey', (req, res) => { // visit http://localhost:8080/student/resetpw
    console.log(req.params.ResetKey);
    user.findOne({ resetKey: req.params.ResetKey }).exec()
        .then(doc => {
            if (doc) {
                res.render('../website/resetpw', { user_id: doc._id, ResetKey: req.params.ResetKey, username: doc.username });
                // will serve views/resetpw.ejs file
            }
            else {
                res.send("Invalid or expired url");
            }
        })
});

app.get('/website/forgotpw', (req, res) => {
    res.render('forgotpw');
});

app.post('/website/forgotpw', (req, res) => {
    user.findOne({ officialEmail: req.body.officialEmail })
        .exec()
        .then(users => {
            if (users) {
                var ResetKey = makeid();
                var mailOptions = {
                    from: 'banadody25@gmail.com',
                    to: req.body.officialEmail,
                    subject: 'Password reset link for UTM Smart 2.0',
                    text: 'Please click this link http://localhost:5100/website/resetpw/' + ResetKey
                };
                console.log(mailOptions);
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                        res.render('forgotpw', { message: "Failed." });
                    } else {
                        console.log('Email sent: ' + info.response);
                        console.log(req.body.officialEmail)
                        user.updateOne({ officialEmail: req.body.officialEmail }, { $set: { resetKey: ResetKey } }).exec();
                        // return res.status(200).json({ message: "Email Sent." });
                        res.render('forgotpw', { message: "Reset password email sent." });
                    }
                });
            }
            else {
                // return res.status(200).json({ message: "Email not found." });
                res.render('forgotpw', { message: "Please enter the oficial email registered with UTM, please contact CICT if you do not remember it." });
            }
        })
})


// Routes for Websites /////////////////////////////////////////////////////////
app.get('/', (req, res) => {
    let sess = req.session;
    if (sess.username) {
        // return res.redirect to dashboard;
        if (sess.userRole == 'student') {
            res.render('dash-student', { info: sess });
        }
        if (sess.userRole == 'lecturer') {
            res.render('dash-lecturer', { info: sess });
        }
        if (sess.userRole == 'staff') {
            res.render('dash-staff', { info: sess });
        }
    } else {
        res.render('home', { errors: {} });
    }
});

// app.post('/', (req, res) => {
//     let sess = req.session;
//     sess.userRole = req.body.userRole;
//     console.log(sess.userRole);
//     if (sess.username) {
//         // return res.redirect to dashboard;
//         if (sess.userRole == 'student') {
//             res.render('dash-student', { info: sess });
//         }
//         if (sess.userRole == 'lecturer') {
//             res.render('dash-lecturer', { info: sess });
//         }
//         if (sess.userRole == 'staff') {
//             res.render('dash-staff', { info: sess });
//         }
//     } else {
//         res.render('login', { errors: {} });
//     }
// });

// app.get('/website/login', (req, res) => {
//     let sess = req.session;
//     if (sess.username) {
//         // return res.redirect to dashboard;
//         if (sess.userRole == 'student') {
//             res.render('dash-student', { info: sess });
//         }
//         if (sess.userRole == 'lecturer') {
//             res.render('dash-lecturer', { info: sess });
//         }
//         if (sess.userRole == 'staff') {
//             res.render('dash-staff', { info: sess });
//         }
//     } else {
//         res.render('home', { errors: {} });
//     }
// });

const { check, validationResult } = require('express-validator');
// const { body } = require('express-validator');

app.post('/website/login', [
    check('username', 'Username is required').not().isEmpty(),
    check('password', 'Password is required').not().isEmpty(),
], (req, res) => {
    let sess = req.session;
    const result = validationResult(req);
    // console.log(result);
    // var errors = result.errors;
    // console.log(errors);
    if (!result.isEmpty()) {
        let arrayErros = result.mapped()
        console.log(arrayErros)
        res.render('login', { errors: arrayErros })
    }
    else {
        console.log("Logging in");
        // Find whether the username and password matches
        user.findOne({ username: req.body.username, password: req.body.password })
            .exec()
            .then(users => {
                if (users) {
                    // Update last login attribute 
                    var now = Date.now()
                    user.updateOne({ _id: users._id }, { $set: { lastLogin: now } }).exec()
                    userRole.findOne({"_id": users.userRole_id}).exec().then(userRoles => {
                        if (userRoles.role == 'student') {
                            student.findOne({ user_id: users._id }).exec()
                                .then(stud => {
                                    console.log(stud, users._id)
                                    if (stud) {
                                        // Store username into session
                                        console.log("Login Session: ", sess);
                                        sess.userRole = userRoles.role;
                                        sess.user_id = users._id;
                                        sess.username = req.body.username;
                                        sess.name = stud.name;
                                        sess.matricNo = stud.matricNo;
                                        return res.redirect('/website/dashboard');
                                    }
                                });
                        }
                        if (userRoles.role == 'lecturer') {
                            lecturer.findOne({ user_id: users._id }).exec()
                                .then(lect => {
                                    console.log(lect, users._id)
                                    if (lect) {
                                        // Store username into session
                                        console.log("Login Session: ", sess);
                                        sess.userRole = userRoles.role;
                                        sess.user_id = users._id;
                                        sess.username = req.body.username;
                                        sess.name = lect.name;
                                        return res.redirect('/website/dashboard');
                                    }
                                });
                        }
                        if (userRoles.role == 'staff') {
                            staff.findOne({ user_id: users._id }).exec()
                                .then(staf => {
                                    console.log(staf, users._id)
                                    if (staf) {
                                        // Store username into session
                                        console.log("Login Session: ", sess);
                                        sess.userRole = userRoles.role;
                                        sess.user_id = users._id;
                                        sess.username = req.body.username;
                                        sess.name = staf.name;
                                        return res.redirect('/website/dashboard');
                                    }
                                });
                        }
                    })
                }
                else {
                    // return res.status(401).json({ message: "Auth failed." });
                    //return res.status(401).json({ message: "Username / password invalid." });
                    console.log("Auth failed");
                    res.render('home', { errors: {msg: "Username / password is invalid."} });
                }
            })
            .catch(err => {
                console.log(err);
                // res.status(500).json({ error: err });
                res.render('home', { errors: {} });
            });
    }
});

app.get('/website/dashboard', (req, res) => {
    let sess = req.session;
    console.log("Dashboard sess", sess)
    if (sess.username) {
        let msg = sess.message;
        sess.message = "";
        if(sess.userRole == 'student'){
            res.render('dash-student', { info: sess, message: msg });
        }
        if(sess.userRole == 'lecturer'){
            res.render('dash-lecturer', { info: sess, message: msg });
        }
        if(sess.userRole == 'staff'){
            res.render('dash-staff', { info: sess, message: msg });
        }
    }
    else {
        res.render('home', { errors: {} });
    }
});

app.get('/website/profile', (req, res) => {
    let sess = req.session;
    console.log("Dashboard sess", sess)
    if (sess.username) {
        let msg = sess.message;
        sess.message = "";
        if(sess.userRole == 'student'){
            res.render('profile-student', { info: sess, message: msg });
        }
        if(sess.userRole == 'lecturer'){
            res.render('profile-lecturer', { info: sess, message: msg });
        }
        if(sess.userRole == 'staff'){
            res.render('profile-staff', { info: sess, message: msg });
        }
    }
    else {
        res.render('home', { errors: {} });
    }
});

app.get('/website/changepw', (req, res) => {
    let sess = req.session;
    console.log("GET changepw:", sess)
    if (sess.username) {
        res.render('changepw', { message: "" });
    }
    else {
        res.render('home', { errors: {} });
    }
});

app.post('/website/changepw', (req, res) => {
    console.log(req.body);
    let sess = req.session;
    console.log("POST changepw:", sess)
    // console.log(sess.username)
    user.findOne({ username: sess.username, password: req.body.password })
        .exec()
        .then(users => {
            if (users) {
                user.updateOne({ username: sess.username }, { $set: { password: req.body.newpassword } }).exec();
                // res.render('stud_dashboard', { info: sess, message: "Successfully changed password." });
                sess.message = "Successfully changed password.";
                return res.redirect('/website/dashboard');
            }
            else {
                res.render('changepw', { message: "Wrong password" });
            }
        })
        .catch(err => {
            console.log(err);
            res.render('changepw', { message: "Auth failed. " });
        });
});

app.get('/website/logout', (req, res) => {
    req.session.destroy((err) => {
        return res.redirect('/');
    })
});


// listen (start app with node server.js)
app.listen(5100, () => console.log('app listening on port 5100'));